import {Component, OnInit} from '@angular/core';
import {TeliaService} from "./services/telia.service";
import {MobileProductOffering, Offerings} from "./models/offering";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {DetailsDialogComponent} from "./details-dialog/details-dialog.component";
import {of} from "rxjs";
import {ScoreResult} from "./models/score";
import {mapTo} from "rxjs/operators";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    loading = false;
    acceptedScore = false;
    isLinear = true;
    readonly = true;
    contractFormGroup: FormGroup;
    SSNFormGroup: FormGroup;
    PersonalInfoGroup: FormGroup;
    PhoneFormGroup: FormGroup;
    a: FormGroup;

    offerings: MobileProductOffering[] = [];
    selectedOffering: MobileProductOffering | undefined;

    constructor(private telia: TeliaService, private _formBuilder: FormBuilder, public dialog: MatDialog) {
    }

    get selectedCode(): string {
        return this.contractFormGroup.getRawValue().productOfferingCode;
    }

    ngOnInit(): void {
        this.initForm();
        this.getOfferings();
    }

    getOfferings() {
        this.loading = true;
        this.telia.getOfferings().subscribe((res: Offerings) => {
            this.offerings = res.mobileProductOfferings;
            this.loading = false;
        })
    }

    bookNumbers() {
        this.loading = true;
        this.telia.bookNumber(1).subscribe((res) => {
            this.loading = false;
            this.PhoneFormGroup.patchValue({
                    number: res.msisdnList[0]
                }
            )
        })
    }

    detailsOffering(offering: MobileProductOffering) {
        this.openDialog(offering)
    }

    getDataBySSN(): void {
        this.acceptedScore = false;
        this.loading = true
        const ssnId = this.SSNFormGroup.getRawValue().ssnId;
        this.telia.score(ssnId)
            .pipe(mapTo(res => {
                if (res.result === ScoreResult.accepted) {
                    return this.telia.getCustomerInstance(ssnId)
                }
                return of(false)
            }))
            .subscribe(res => {
                if (res) {
                    this.acceptedScore = true;
                    this.telia.getCustomerInstance(ssnId).subscribe(res => {
                        this.PersonalInfoGroup.patchValue({
                            firstname: res.firstName,
                            lastname: res.lastName,
                            address: res.address,
                            city: res.city,
                            mobilePhoneNumber: res.contactPhoneNumber,
                            email: res.email,
                            zipCode: res.zip,
                        });
                    });
                }
                this.loading = false
            })
    }

    openDialog(offering: MobileProductOffering): void {
        const dialogRef = this.dialog.open(DetailsDialogComponent, {
            data: offering
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.contractFormGroup.patchValue({productOfferingCode: result})
            }
        });
    }

    order() {
        const offering = <MobileProductOffering>this.offerings.find(offering => offering.productOfferingCode === this.selectedCode)
        const order = {
            customerInformation: {
                legalId: this.SSNFormGroup.getRawValue().ssnId,
                contactPhoneNumber: this.PersonalInfoGroup.getRawValue().PersonalInfoGroup,
                contactEmail: this.PersonalInfoGroup.getRawValue().email,
            },
            orderItems: [
                {
                    productOffering: {
                        productOfferingCode: offering.productOfferingCode,
                        serviceCode: offering.primaryLine.serviceCode,
                        categorySpecificAttributes: {
                            subscriptionId: this.PhoneFormGroup.getRawValue().number,
                        }
                    }
                }
            ]
        }
        this.telia.placeOrder(order).subscribe(res => {
            console.log(res)
        })
    }

    private initForm(): void {
        this.contractFormGroup = this._formBuilder.group({
            productOfferingCode: ['', Validators.required]
        });
        this.SSNFormGroup = this._formBuilder.group({
            ssnId: ['', Validators.required]
        });
        this.PersonalInfoGroup = this._formBuilder.group({
            firstname: ["", Validators.required],
            lastname: ["", Validators.required],
            address: ["", Validators.required],
            city: ["", Validators.required],
            mobilePhoneNumber: ["", Validators.compose([Validators.required])],
            email: ["", [Validators.required, Validators.email]],
            zipCode: ["", Validators.required],
        });
        this.PhoneFormGroup = this._formBuilder.group({
            number: ["", Validators.required]
        })
    }


}
