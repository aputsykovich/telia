import {Component, Inject, OnInit} from '@angular/core';
import {MobileProductOffering} from "../models/offering";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {FlatTreeControl} from "@angular/cdk/tree";
import {MatTreeFlatDataSource, MatTreeFlattener} from "@angular/material/tree";

interface Node {
    name: string;
    children?: Node[];
}
/** Flat node with expandable and level information */
interface ExampleFlatNode {
    expandable: boolean;
    name: string;
    level: number;
}

@Component({
    selector: 'app-details-dialog',
    templateUrl: './details-dialog.component.html',
    styleUrls: ['./details-dialog.component.scss']
})
export class DetailsDialogComponent implements OnInit {
  private _transformer = (node: Node, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
    };
  }
  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.children);
  treeControl = new FlatTreeControl<ExampleFlatNode>(
        node => node.level, node => node.expandable);
    dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    constructor(@Inject(MAT_DIALOG_DATA) public data: MobileProductOffering) {
        this.dataSource.data = [this.transform(this.data)];
    }

    hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

    ngOnInit(): void {

    }

    transform(obj: Object, name:string = 'Product information'): Node {
        const root: Node = {name, children: []}
        for (let prop in obj) {
          if(typeof obj[prop] === 'object') {
            root.children.push(this.transform(obj[prop], prop))
          } else {
            root.children.push({name: `${prop} : ${obj[prop]}`})
          }
        }
        return root;
    }

}
