export interface AuthResponse {
    refresh_token_expires_in: number;
    api_product_list: Array<string>;
    api_product_list_json: Array<string>;
    organization_name: string;
    'developer.email': string;
    token_type: string;
    issued_at: string;
    client_id: string;
    access_token: string;
    application_name: string;
    scope: string;
    expires_in: number;
    refresh_count: number;
    status: string;
}