export interface BookedNumber {
    amountOfReturnedNumbers: number,
    msisdnList: string[],
    returnCode: string
}