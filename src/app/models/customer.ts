export interface Customer {
    legalId?: number,
    customerType?: string,
    contactPhoneNumber: string,
    contactEmail: string,
    SSN?: number,
    identification?: {
        method: string
    }
}