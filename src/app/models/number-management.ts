
export interface BookingNumber {
    searchString: string,
    numberType: string,
    amountOfNumbers: number,
    productOfferingCode: string,
    serviceCode: string
}