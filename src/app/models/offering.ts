import {ItemPaymentType, ItemProductCategory} from "./score";

export interface OfferingsRequest {
    customerType?: string,
    categories?: string,
    brand?: string,
    offeringTargetSegment?: string
}

export interface Offerings {
    mobileProductOfferings: MobileProductOffering[]
}

export interface MobileProductOffering {
    offeringCategory: string,
    financeAllowed: boolean,
    productOfferingCode: string,
    primaryLine: Line,
    displayName: string,
    brand: string,
    paymentType: ItemPaymentType,
    crm: string,
    supplementaryLines: Line[],
    salesChannels: string[],
    offeringTargetSegment: string,
    associatedOfferings: AssociatedOffering[]
}

export interface Line {
    serviceCode: string,
    category: ItemProductCategory,
    subscriptionCategory: string,
    subCategory: string,
    displayName: string,
    startupFee: Fee,
    fixedFee: Fee,
    orderQuantity?: Quantity,
    orderAttributes?: string[],
    commitments: Commitment[],
    salesChannels: string[],
    valueClass: number,
    dataSize?: DataSize,
}

export interface AssociatedOffering {
    promotionCode: string,
    associatedType: string
}

export interface Fee {
    valueInclVAT: number,
    valueExclVAT: number,
    type: string
}

export interface Quantity {
    default: number,
    max: number,
    min: number
}

export interface Commitment {
    displayName: string,
    period: number
}

export interface DataSize {
    value: string,
    sort: String,
    period: String
}