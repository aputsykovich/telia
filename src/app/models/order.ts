import {Customer} from "./customer";

export interface Order {
    orderType?: string,
    deliveryDate?: string,
    customerInformation?: Customer,
    orderItems?: OrderItem[]
}

export interface OrderItem {
    productOffering?: {
        productOfferingCode?: string,
        serviceCode?: string,
        categorySpecificAttributes?: {
            subscriptionId?: string,
            commitmentPeriod?: number
        }
    }
}