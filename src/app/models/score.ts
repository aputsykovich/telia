export enum ScoreActionType {
    New = 'NEW', //TODO add
}

export enum ScoreCustomerType {
    Consumer = 'CONSUMER', //TODO add
}

export enum ItemProductCategory {
    mobileVS = 'mobilevoicesubscription',
}

export enum ItemProductSubCategory {
    voice = 'voice',
}

export enum ItemPaymentType {
    postPaid = 'POST_PAID',
}

export enum ScoreResult {
    accepted = 'ACCEPTED',
}

export enum ScoringResult {
    REJECTED_CREDITCHECK,
    REJECTED_BUSINESSRULES,
    MANUAL,
    ACCEPTED,
    REJECTED_BLACKLIST
}

export interface ScoreOrderItem {
    productCategory: ItemProductCategory,
    productSubCategory: ItemProductSubCategory,
    paymentType: ItemPaymentType,
    commitmentExtension: boolean,
    actionType: ScoreActionType
}

export interface ScoreRequest {
    tcadId: string,
    legalId: number,
    customerType: ScoreCustomerType,
    teliaFinance: boolean,
    orderItems: ScoreOrderItem[]
}

export interface ScoreResponse {
    result: ScoreResult,
    description: string,
    customerHoldingQuantity: number,
    validQuantity: number
}
