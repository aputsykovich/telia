export interface TeliaCustomerInstance {
    address: string
    address2: string
    city: string
    corporateName: ""
    contactPhoneNumber: string,
    customerType: string
    firstName: "Zakay"
    lastName: "Danial",
    tscId: string,
    legalId: string,
    zip: string,
    email: string
}