import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {Offerings} from "../models/offering";
import {ScoreResponse, ScoreResult} from "../models/score";
import {TeliaCustomerInstance} from "../models/telia-customer-instance";
import {BookedNumber} from "../models/booked-number";
import {Order} from "../models/order";

@Injectable({
  providedIn: 'root'
})
export class TeliaService {
  private readonly api = environment.api;

  constructor(private http: HttpClient) { }

  getOfferings():Observable<Offerings> {
    return this.http.get<Offerings>(this.api + 'offerings');
  }

  bookNumber(amount: number): Observable<BookedNumber> {
    return this.http.get<BookedNumber>(this.api + `book-number?amount=${amount}`);
  }

  placeOrder(order: Partial<Order>): Observable<any> {
    return this.http.post<any>(this.api + 'order/new', order)
  }

  score(SSN: number):Observable<ScoreResponse> {
    return this.http.get<ScoreResponse>(this.api + `score/${SSN}`);
  }

  getCustomerInstance(SSN: number): Observable<TeliaCustomerInstance> {
    return this.http.get<TeliaCustomerInstance>(this.api + `instance/${SSN}`);
  }

}
